package main

import "fmt"
import "encoding/csv"
import "os"
import "strings"
import "sort"
import "strconv"
import "bufio"
import "text/tabwriter"

func main() {

	// Open the file
	csvfile, err := os.Open("car_orders.csv")
	if err != nil {
		fmt.Printf("Couldn't open the csv file", err)
	}

	// Read file
	r := csv.NewReader(csvfile)
	lines, err := r.ReadAll()

	if err != nil {
		fmt.Printf("fatal error %s", err)
	}
		
	// Create the map for counting orders per company
	var company_orders map[int]int
	company_orders = make(map[int]int)
	
	// Create the map for counting cars
	var car_count map[string]int
	car_count = make(map[string]int)
	
	// Create the map for counting cars per company
	var company_cars map[int]map[string]int
	company_cars = make(map[int]map[string]int)
	
	// Read all lines, except first as its just the header
	for i := 1; i < len(lines); i++ {

		// Split each row
		row := strings.Split(strings.Join(lines[i], ""), ";") 

		// Get companyid and car 
		company, _ := strconv.Atoi(row[0]);
		var car = row[2];
		
		// Add to company_orders map
		if _, ok := company_orders[company]; ok {
			company_orders[company]++
		} else {
			company_orders[company] = 1
		}
		
		// Add to car_count map
		if _, ok := car_count[car]; ok {
			car_count[car]++
		} else {
			car_count[car] = 1
		}
		
		// Create map for each company to keep track of cars per company
		if _, ok := company_cars[company]; !ok {
			company_cars[company] = make(map[string]int)
		}

		// Nested map, count cars per company
		if _, ok := company_cars[company][car]; ok {
			company_cars[company][car]++
		} else {
			company_cars[company][car] = 1
		}
	}
	
	// Sort
	var company_orders_sorted = SortIntMapDescending(company_orders);
	var car_count_sorted = SortStringMapDescending(car_count);
	var most_popular_cars = GetPopularCarsPerCompany(company_cars)
	
	// Display menu
	reader := bufio.NewReader(os.Stdin)

	for {
	
		fmt.Println("-------------------------------")
		fmt.Println("1. Print orders per company")
		fmt.Println("2. Print car count")
		fmt.Println("3. Print most popular car(s) per company")
		fmt.Println("4. Exit")
		
		input, _ := reader.ReadString('\n')
	
	    // convert CRLF to LF (if on Windows)
		input = strings.Replace(input, "\r\n", "", -1)
		
		switch(input) {
			case "1":
				PrintOrdersPerCompany(company_orders_sorted)
			case "2":
				PrintCarCount(car_count_sorted)
			case "3":
				PrintMostPopularCarPerCompany(most_popular_cars)
			case "4":
				return
		}
	}
}


func PrintCarCount(p PairListStringInt) {

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	fmt.Fprintf(w, "%s\t%s\t\n", "Car", "Count")
	fmt.Fprintf(w, "%s\t%s\t\n", "---", "-----")
	
	for _, k := range p {
		fmt.Fprintf(w, "%s\t%d\t\n", k.Key, k.Value)
	}
	w.Flush()
}

func PrintMostPopularCarPerCompany(m map[int]PairListStringInt) {

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	fmt.Fprintf(w, "%s\t%s\t\n", "Company", "Most popular car(s)")
	fmt.Fprintf(w, "%s\t%s\t\n", "-------", "-------------------")
	
	for key, value := range m {
	
		var largest = 0
		var cars = ""
		for _, k := range value {
		
			// Only print out the most popular car(s)
			if(largest == 0) {
				largest = k.Value;
			} else if(k.Value < largest) {
				break
			}
			
			if(len(cars) > 0) {
				cars += ", "
			}
				
			cars += k.Key
		}
		fmt.Fprintf(w, "%d\t%s\t\n", key, cars)
	}
	w.Flush()
}

func PrintOrdersPerCompany(p PairListIntInt) {

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 8, 8, 0, '\t', 0)
	fmt.Fprintf(w, "%s\t%s\t\n", "Company", "Orders")
	fmt.Fprintf(w, "%s\t%s\t\n", "-------", "------")
	
	for _, k := range p {
		fmt.Fprintf(w, "%d\t%d\t\n", k.Key, k.Value)
	}
	w.Flush()
}

func GetPopularCarsPerCompany(m map[int]map[string]int) map[int]PairListStringInt {

	// Create a new map, which will contain companies and a sorted list
	var company_cars map[int]PairListStringInt
	company_cars = make(map[int]PairListStringInt)
	
	for key, value := range m {
	
		company_cars[key] = SortStringMapDescending(value);
	}

	return company_cars;
}

func SortIntMapDescending(m map[int]int) PairListIntInt {

	p := make(PairListIntInt, len(m))
	i := 0
	for k, v := range m {
		p[i] = PairIntInt{k, v}
		i++
	}
	sort.Sort(p)
	return p
}

func SortStringMapDescending(m map[string]int) PairListStringInt {

	p := make(PairListStringInt, len(m))
	i := 0
	for k, v := range m {
		p[i] = PairStringInt{k, v}
		i++
	}
	sort.Sort(p)
	return p
}

type PairIntInt struct {
	Key   int
	Value int
}

type PairStringInt struct {
	Key   string
	Value int
}


type PairListIntInt []PairIntInt
func (p PairListIntInt) Len() int           { return len(p) }
func (p PairListIntInt) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairListIntInt) Less(i, j int) bool { return p[i].Value > p[j].Value }

type PairListStringInt []PairStringInt
func (p PairListStringInt) Len() int           { return len(p) }
func (p PairListStringInt) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }
func (p PairListStringInt) Less(i, j int) bool { return p[i].Value > p[j].Value }

